# [SECTION] Lists

names = ["John", "Paul", "George", "Ringo"]
programs = ["developer career", "pi-shape", "short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

#length of list
print(len(programs))

#item from the list
print(programs[0])

#starting index:length
print(programs[0:2])

#updating an item in the list
programs[2] = "Short Courses"

print(programs[2])

# [SECTION] List Manipulation

#adding an item to the list
durations.append(367)
print(durations)

#deleting an item from the list
del durations[-1]
print(durations)

names.sort()
print(names)

#clearing or emptying the list
test_list = [1, 3, 5, 7, 9]
test_list.clear()
print(test_list)

# [SECTION] Dictionaries
person1 = {
	"name": "Elon",
	"age": 70,
	"occupation": "student",
	"isEnrolled": True,
	"subject": ["Python", "SQL", "Django"]
}

#lenght of dictionary
print(len(person1))

#specific value based on the key
print(person1["name"])

#all keys
print(person1.keys())

#all values
print(person1.values())

#all items
print(person1.items())


# [SECTION] Dictionary Manipulation

person1["nationality"] = "Norwegian"
person1.update({"fave_food": "Diniguan"})
print(person1)

#deleting can either be done by using pop() or the del keyword
person1.pop("fave_food")
del person1["nationality"]
print(person1)

#clearing all entries
person2 = {
	"name": "Mystika",
	"age": 18
}

person2.clear()
print(person2)

person3 = {
	"name": "Monika",
	"age": 21,
	"occupation": "lyricist",
	"isEnrolled": True,
	"subject": ["Python", "SQL", "Ruby"]
}

classRoom = {
	"student1": person1,
	"student2": person3
}


# [SECTION] Functions

def my_greeting():
	print("Hello user!")

my_greeting()

def greet_user(username):
	print(f"Hello, {username}")

greet_user("Elon")

# Lambda Functions
greeting = lambda person : f"Hello person! {person}"
print(greeting("Elsie"))
print(greeting("Tony"))

# [SECTION] Classes
class Car ():
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# Other properties
		self.fuel = "Gasoline"
		self.fuel_level = 0

	def fill_fuel(self):
		print(f"Current fuel level: {self.fuel_level}")
		print('... filling up the fuel tank')
		self.fuel_level = 100
		print(f"New fuel level: {self.fuel_level}")


# Mini activity
# Declare a drive function which displays total distance driven

	def drive(self, distance):
		print(f"The car has driven {distance} km.")
		print(f"The fuel level left: {self.fuel_level - distance}")









new_car = Car ("Maserati", "Explorer", "2005")

print(f"My car is a {new_car.brand} {new_car.model}")

new_car.fill_fuel()
new_car.drive(50)


